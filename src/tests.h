#ifndef TESTS_H
#define TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdbool.h>
#include <sys/mman.h>

bool test1();
bool test2();
bool test3();
bool test4();
bool test5();

#endif
